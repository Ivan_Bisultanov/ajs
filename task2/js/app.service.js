(function () {
    'use strict';

    angular
        .module('app')
        .service('AppService', AppService);

    AppService.$inject = ['AppFactory'];

    function AppService(AppFactory) {

        var self = this;

        self.getTabContent = function (tab) {
            return AppFactory().getTabContent(tab);
        };

    }

}());