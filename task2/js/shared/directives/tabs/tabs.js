(function () {
    'use strict';

    angular
        .module('tabs', [])
        .directive('tabs', Tabs);

    function Tabs() {
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'js/shared/directives/tabs/tabs.html',
            scope: {
                tabs: '=tabsObj'
            }
        };
    }

})();