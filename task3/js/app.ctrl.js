(function () {

    'use strict';

    angular
        .module('app')
        .controller('AppCtrl', AppCtrl);

    AppCtrl.$inject = ['AppService'];

    function AppCtrl(AppService) {
        var vm = this;

        vm.actions = {
            loadTabContent: loadTabContent
        };
        vm.local = {
            currentTabContent: vm.actions.loadTabContent('London')
        };
        vm.tabsObj = {
            tabTextArr: ['tab0', 'tab1', 'tab2'],
            tabCityArr: ['London', 'Minsk', 'Paris'],
            callback: loadTabContent
        };

        function loadTabContent(tab) {
            AppService.getTabContent(tab).then(function (dataResponse) {
                vm.local.currentTabContent = dataResponse.data;
            });
        }
    }

})();