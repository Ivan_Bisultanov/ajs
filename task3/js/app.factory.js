(function () {
    'use strict';

    angular
        .module('app')
        .factory('AppFactory', AppFactory);

    AppFactory.$inject = ['$http'];

    function AppFactory($http) {
        return function () {
            return {
                getTabContent: function (tab) {
                    return $http({
                        method: 'GET',
                        url: 'http://api.openweathermap.org/data/2.5/weather?q=' + tab + '&appid=1472985b7d76391a3621f8f07ae067f9'
                    });
                }
            };
        };
    }

}());
