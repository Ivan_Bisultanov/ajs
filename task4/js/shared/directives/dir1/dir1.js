(function () {
    'use strict';

    angular
        .module('dir1', [])
        .directive('dirOne', Dir1);

    function Dir1() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'js/shared/directives/dir1/dir1.html',
            controller : function($scope){
                this.data = 'test value';
                this.setData = function(){
                    $scope.$emit('updateValue', this.data);
                };
            },
            controllerAs : 'firstDirCtrl'
        };
    }

})();

