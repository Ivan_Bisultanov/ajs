(function () {
    'use strict';

    angular
        .module('dir2', [])
        .directive('dirTwo', Dir2);

    function Dir2() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'js/shared/directives/dir2/dir2.html',
            controller : function($rootScope){
                var that = this;
                $rootScope.$on('updateValue', function(e, data){
                    that.data = data;
                });
                this.data = 'test value';
            },
            controllerAs : 'secondCtrl'
        };
    }

})();
