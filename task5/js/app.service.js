(function () {
    'use strict';

    angular
        .module('app')
        .service('AppService', AppService);

    function AppService() {

        var self = this;

        self.testValue = 'test value';
        self.callbacks = [];

        self.registerCallback = function(callback){
            self.callbacks.push(callback);
        };
        self.notifyObservers = function(){
            angular.forEach(self.callbacks, function(callback){
                callback();
            });
        };

    }

}());