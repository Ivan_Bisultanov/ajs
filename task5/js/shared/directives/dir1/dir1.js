(function () {
    'use strict';

    angular
        .module('dir1', []).
        directive('dirOne', Dir1);

    Dir1.$inject = ['AppService'];

    function Dir1() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'js/shared/directives/dir1/dir1.html',
            controller : function($scope, AppService){
                this.data = AppService.testValue;
                this.setData = function(){
                    AppService.testValue = this.data;
                    AppService.notifyObservers();
                };
            },
            controllerAs : 'firstDirCtrl'
        };
    }

})();

