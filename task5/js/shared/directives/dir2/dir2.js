(function () {
    'use strict';

    angular
        .module('dir2', [])
        .directive('dirTwo', Dir2);

    Dir2.$inject = ['AppService'];

    function Dir2() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'js/shared/directives/dir2/dir2.html',
            controller : function($rootScope, $scope, AppService){
                var that = this;
                that.data = AppService.testValue;
                that.updateValue = function(){
                    that.data = AppService.testValue;
                };
                AppService.registerCallback(that.updateValue);
            },
            controllerAs : 'secondCtrl'
        };
    }

})();
