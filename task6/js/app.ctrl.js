(function () {

    'use strict';

    angular
        .module('app')
        .controller('AppCtrl', AppCtrl);

    AppCtrl.$inject = ['$timeout'];

    function AppCtrl($timeout) {
        var vm = this;

        vm.local = {
            testValue: new Date()
        };

        vm.method$timeout = function () {
            $timeout(function () {
                vm.local.testValue = new Date();
            }, 1500);
        }();
        vm.methodSetTimeout = function () {
            setTimeout(function () {
                vm.local.testValue = new Date();
            }, 3000);
        }();
    }

})();