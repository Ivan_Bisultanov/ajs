(function () {

    'use strict';

    angular
        .module('app')
        .controller('AppCtrl', AppCtrl);

    AppCtrl.$inject = ['$timeout', '$http'];

    function AppCtrl($timeout, $http) {
        var vm = this;

        vm.local = {
            testValue: new Date()
        };

        vm.method$timeout = function () {
            $timeout(function () {
                $http.get('https://angularjs.org/greet.php?callback=JSON&name=TAB').success(function(dataResponse) {
                    vm.local.testValue = JSON.parse(dataResponse.substring(6, dataResponse.length - 2)).greeting;
                });
            }, 1500);
        }();
        vm.methodSetTimeout = function () {
            setTimeout(function () {
                $http.get('https://angularjs.org/greet.php?callback=JSON&name=TAB').success(function(dataResponse) {
                    vm.local.testValue = JSON.parse(dataResponse.substring(6, dataResponse.length - 2)).greeting;
                });
            }, 3000);
        }();
    }

})();