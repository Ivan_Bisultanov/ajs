(function () {

    'use strict';

    angular
        .module('app', ['sbox'])
        .controller('AppCtrl', AppCtrl);

    function AppCtrl($scope) {
        var vm = this;
        vm.selectObj = {
            options: [
                {
                    value: 1,
                    text: 'one'
                }, {
                    value: 2,
                    text: 'two'
                }, {
                    value: 3,
                    text: 'three'
                }, {
                    value: 4,
                    text: 'four'
                }
            ],
            id: 'selectBox'
        };
        $scope.test = this;
    }


    describe('app', function () {
        var scope,
            controller;
        beforeEach(function () {
            module('app');
        });

        describe('AppCtrl', function () {
            beforeEach(inject(function ($rootScope, $controller) {
                scope = $rootScope.$new();
                controller = $controller('AppCtrl', {
                    '$scope': scope
                });
            }));
            it('sets the name', function () {
                expect(scope.test.selectObj.options[0].value).toBe(1);
            });

        });

    });
})();

