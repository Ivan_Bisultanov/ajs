(function () {
    'use strict';

    angular
        .module('sbox', []).
    directive('sbox', SBox);

    SBox.$inject = ['$timeout'];

    function SBox($timeout) {
        return {
            restrict: 'E',
            scope: {
                sboxObj: '=sbox'
            },
            templateUrl: 'js/shared/directives/sbox/sbox.html',
            link: function (scope) {
                var sboxSelector = '#' + scope.sboxObj.id;
                angular.element(document).on('change', sboxSelector, function () {
                    scope.money = angular.element(this).val();
                    scope.$apply();
                });
                scope.money = scope.sboxObj.options[0].value;

                $timeout(function () {      // реально добиться чтобы срабатывала функция без таймаута?
                    angular.element(sboxSelector).SBox();  // как отследить момент когда директива все отрендерила?
                }, 0);
            }
        };
    }

    //describe("Directive", function () {
    //
    //    var $scope;
    //
    //    beforeEach(inject(function($rootScope, $compile) {
    //            $scope = $rootScope.$new();
    //            var element = angular.element("<sbox></sbox>"),
    //            template = $compile(element)($scope);
    //            $scope.$digest();
    //            var controller = element.controller;
    //         }));
    //
    //    it("should toogle open when toggle() is called", inject(function() {
    //            $scope.money = 1;
    //            expect($scope.money).toBe(1);
    //        }));
    //
    //});

})();

