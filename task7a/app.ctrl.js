(function () {

    'use strict';

    angular
        .module('app', ['sbox'])
        .controller('AppCtrl', AppCtrl);

    function AppCtrl($scope) {
        var vm = this;
        vm.selectObj = {
            options: [
                {
                    value: 1,
                    text: 'one'
                }, {
                    value: 2,
                    text: 'two'
                }, {
                    value: 3,
                    text: 'three'
                }, {
                    value: 4,
                    text: 'four'
                }
            ],
            id: 'selectBox'
        };
        $scope.test = this;
    }

})();

