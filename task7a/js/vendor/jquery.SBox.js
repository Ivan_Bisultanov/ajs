$.fn.SBox = function(options) {
    this.default = {
        dataSelect: false             //add data-select="" to option list if text, val and text selected option different
    };
    $.extend(this.default, options);

    var nativeSelect = this,
        selectId = nativeSelect.attr('id'),
        SBoxSelector = '#'+selectId,
        parentClass = selectId + '-ibsb-wrap',
        $SBoxSelect = $(SBoxSelector),
        $SBox,
        $SBoxUl,
        $SBoxSelected,
        $ibSboxOption,
        $ibSboxOptionSelected,
        optionList = [],
        optionListLength,
        counter = 0,
        i;

    $SBoxSelect.hide().wrap("<div class='SBox-wrap " + parentClass + "'></div>");
    $SBox = $('.' + parentClass);

    $SBoxSelect.find('option').each(function () {
        var $that = $(this),
            optionText = $that.text(),
            optionVal = $that.val(),
            optionSelectText = $that.data('select');
        optionList[counter] = {};
        optionList[counter].text = optionText;
        optionList[counter].val = optionVal;
        optionList[counter].select = optionSelectText;
        counter += 1;
    });

    $ibSboxOptionSelected = $SBox.find('option:selected');
    if (nativeSelect.default.dataSelect) {
        $SBox.append('<div class="' + 'SBox-selected' + '" data-value="' + $ibSboxOptionSelected.val() + '">' + $ibSboxOptionSelected.data('select') + '</div>');
    } else {
        $SBox.append('<div class="' + 'SBox-selected' + '" data-value="' + $ibSboxOptionSelected.val() + '">' + $ibSboxOptionSelected.text() + '</div>');
    }
    $SBoxSelected = $SBox.find('.SBox-selected');
    $SBox.append('<div class="' + 'SBox-ul' + '"></div>');
    $SBoxUl = $SBox.find('.SBox-ul');

    optionListLength = optionList.length;
    for (i = 0; i < optionListLength; i += 1) {
        if (nativeSelect.default.dataSelect) {
            $SBoxUl.append('<div class="SBox-li" data-value="' + optionList[i].val + '" data-select="' + optionList[i].select + '">' + optionList[i].text + '</div>');
        } else {
            $SBoxUl.append('<div class="SBox-li" data-value="' + optionList[i].val + '">' + optionList[i].text + '</div>');
        }
    }
    $ibSboxOption = $SBox.find('.SBox-li');
    $ibSboxOption.eq($ibSboxOptionSelected.index()).addClass('_selected');

    $SBox.on('click.stopProp', function (e) {
        e.stopPropagation();
    });
    $SBoxSelected.on('click.openCloseSBox', function () {
        var $that = $(this);
        $('.SBox-selected').not($that).removeClass('_open').next().fadeOut();
        if ($that.hasClass('_open')) {
            $that.removeClass('_open');
            $SBoxUl.fadeOut();
        } else {
            $that.addClass('_open');
            $SBoxUl.fadeIn().jScrollPane({
                verticalGutter: -8
            });
        }
    });
    $('body').on('click.closeSBox', function () {
        $SBoxSelected.removeClass('_open');
        $SBoxUl.fadeOut();
    });

    $ibSboxOption.on('click.chooseValue', function () {
        var $that = $(this);
        $ibSboxOption.removeClass('_selected');
        $that.addClass('_selected');
        if (nativeSelect.default.dataSelect) {
            $SBoxSelected.text($that.data('select'));
        } else {
            $SBoxSelected.text($that.text());
        }
        $SBoxSelected.attr('data-value', $that.data('value'));
        $('body').trigger('click.closeSBox');
        $SBoxSelect.val($that.data('value'));
        $(document).find(SBoxSelector).trigger('change');
    });

    $(window).on('keydown.SBox', function(e) {
        if (e.keyCode === 27) {
            $('body').trigger('click.closeSBox');
        }
    });

};
