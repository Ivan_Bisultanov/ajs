module.exports = function(config) {
  config.set({
    basePath: '',
    autoWatch: true,
    frameworks: ['jasmine'],
    files: [
      'js/vendor/jquery-2.1.4.min.js',
      'js/vendor/jquery.mousewheel.js',
      'js/vendor/jquery.jscrollpane.min.js',
      'js/vendor/jquery.SBox.js',
      'js/vendor/angular/angular.1.4.8.min.js',
      'bower_components/angular-mocks/angular-mocks.js',
      //'js/shared/directives/sbox/sbox.directive.js',

      'sbox.directive.js',

      'js/app.js',
      //'js/app.ctrl.js',


      'app.ctrl.js',
      'sqrt.js',
      'test/spec/*.js',
      'test/ctrl/*.js',
      'test/dir/*.js'
    ],
    browsers: ['PhantomJS'],

    reporters: ['progress', 'coverage'],
    preprocessors: { '*.js': ['coverage'] },

    singleRun: true
  });
};
