describe('app.ctrl', function () {
    var scope,
        controller;
    beforeEach(function () {
        module('app');
    });

    describe('AppCtrl', function () {
        beforeEach(inject(function ($rootScope, $controller) {
            scope = $rootScope.$new();
            controller = $controller('AppCtrl', {
                '$scope': scope
            });
        }));
        it('sets the name', function () {
            expect(scope.test.selectObj.options[0].value).toBe(1);
        });

    });

});