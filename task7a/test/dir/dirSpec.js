describe("Directive", function () {

    var $scope, controller, template, element;

    beforeEach(function () {
        module('app');
    });
    beforeEach(function() {
        html = '<sbox></sbox>';

        inject(function($compile, $rootScope, $templateCache) {

            $scope = $rootScope.$new();
            $scope.wikis = [];

            elem = angular.element(html);

            $compile(elem)($scope);

            scope = elem.isolateScope();
            scope.$apply();
        });

    });

    it('dirtest', inject(function($rootScope, $compile) {
        element = $compile('<sbox></sbox>')($rootScope);
        $rootScope.$digest();
        console.log(element);
        $rootScope.sboxObj = {
            options: [
                {
                    value: 1,
                    text: 'one'
                }, {
                    value: 2,
                    text: 'two'
                }, {
                    value: 3,
                    text: 'three'
                }, {
                    value: 4,
                    text: 'four'
                }
            ],
            id: 'selectBox'
        };
        console.log($rootScope.sboxObj);
        expect(1).toEqual(1);

        //browserTrigger(element, 'click');
        //expect($rootScope.clicked).toEqual(true);
    }));

});