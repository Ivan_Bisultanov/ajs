(function () {

    'use strict';

    angular.module('app', ["ui.router"])
        .config(function($stateProvider, $urlRouterProvider){

            $urlRouterProvider.when("", "/home/viewA");
            $urlRouterProvider.when("/", "/home/viewA");

            $urlRouterProvider.otherwise("/home/viewA");

            $stateProvider
                .state('home', {
                    abstract: true,
                    url: '/home',
                    templateUrl: 'js/views/view0.html',
                    controller: function($scope){
                        $scope.date = new Date();
                    }
                })
                .state('home.viewA', {
                    url: '/viewA',
                    templateUrl: 'js/views/viewA.html',
                    controller: function($scope){
                        $scope.date = new Date();
                    }
                })
                .state('home.viewB', {
                    url: '/viewB',
                    templateUrl: 'js/views/viewB.html',
                    controller: function($scope){
                        $scope.date = new Date();
                    }
                })
        })

})();