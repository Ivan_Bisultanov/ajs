(function () {

    'use strict';

    angular.module('app', ["ui.router"])
        .config(function($stateProvider, $urlRouterProvider){

            $urlRouterProvider.when("", "/home/viewA");
            $urlRouterProvider.when("/", "/home/viewA");

            $urlRouterProvider.otherwise("/home/viewA");

            $stateProvider
                .state('home', {
                    abstract: true,
                    url: '/home',
                    templateUrl: 'js/views/view0.html',
                    controller: function($scope){
                        $scope.date = new Date();
                    }
                })
                .state('home.viewA', {
                    url: '/viewA',
                    templateUrl: 'js/views/viewA.html',
                    controller: function($scope){
                        $scope.date = new Date();
                    }
                })
                .state('home.viewB', {
                    url: '/viewB',
                    resolve: {
                        promiseObj: function ($http) {
                            return $http({method: 'GET', url: 'https://api.mongolab.com/api/1/databases/fj/collections/articles?s={%22index%22:-1}&l=1&apiKey=uddbELn9HaVkrV8GfgtLB2SbQpEijG6z'});
                        }
                    },
                    templateUrl: 'js/views/viewB.html',
                    controller: function($scope){
                        $scope.date = new Date();
                    }
                })
        })

})();