var example = angular.module('example', ['ui.router']);

example.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
        })
        .state('secure', {
            url: '/secure',
            templateUrl: 'templates/secure.html',
            controller: 'SecureController'
        });

    $urlRouterProvider.otherwise('/login');
});

example.controller('LoginController', function($scope) {
    $scope.login = function() {
        window.location.href = 'https://oauth.vk.com/authorize?client_id=5222893&display=page&redirect_uri=http://localhost:63342/oauthVK/oauth_callback.html&scope=friends&response_type=code&v=5.44';
    };
});

example.controller('SecureController', function($scope, $http) {

    $scope.accessToken = JSON.parse(localStorage.getItem('accessToken'));
    $scope.userInfo = {};
    $http({
        method: 'GET',
        url: 'https://api.vk.com/method/users.get?user_id=' + $scope.accessToken.user_id + '&fields=photo_max_orig' + '&v=5.44&access_token=' + $scope.accessToken.access_token
    }).then(function successCallback(response) {
        $scope.userInfo = response;
    }, function errorCallback(response) {
        console.log(response)
    });

});